module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		watch: {
			files: ['assets/sass/styles.scss','assets/js/*.js'],
			tasks: [ 'cssmin', 'uglify' ]
		},
		cssmin: {
			target: {
				files: {
					'public/css/styles.min.css': 'public/css/styles.css'
				}
			}
		},
		sprite:{
			all: {
				src: ['assets/sprites/*.png'],
				// retinaSrcFilter: ['assets/sprites/*@2x.png'],
				dest: 'public/img/sprites.png',
				// retinaDest: 'public/img/sprites@2x.png',
				destCss: 'assets/sass/_sprites.scss'
			}
		},
		imagemin: {                          // Task
			static: {                          // Target
				files: {                         // Dictionary of files
					'public/img/sprites@2x.png': 'public/img/sprites@2x.png', // 'destination': 'source'
					'public/img/sprites.png': 'public/img/sprites.png'
				}
			}
		},
		uglify: {
			alljs: {
				files: {
					'public/js/app.min.js': ['assets/js/*.js']
				}
			}
		}
	});

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-cssmin');
 	grunt.loadNpmTasks('grunt-contrib-watch');
 	grunt.loadNpmTasks('grunt-spritesmith');
 	grunt.loadNpmTasks('grunt-contrib-uglify');

	// Default task(s).
	grunt.registerTask('default', ['watch']);
	grunt.registerTask('images', ['sprite']);


};