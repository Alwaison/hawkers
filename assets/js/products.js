;
/**
 * Almacenamos el listado de productos en el navegador
 */
jQuery.getJSON("https://bitbucket.org/Alwaison/hawkers/raw/94961f6940540fdb7b2df3f0149566c08e535684/sources/products.json", function(data){
	$.each( data.products, function( key, val ) {
		val['index'] = key;
		window.sessionStorage.setItem(key,JSON.stringify(val));
	});
});

/**
 * Creamos funciones para leer e insertar los productos dinámicamente
 */
var Products = (function(){
	var lastkey = 0; //Último elemento insertado
	// Templates HTML para los productos
	var template = $('script[data-template="list__item"]').text().split(/\$\{(.+?)\}/g);
	var templateBig = $('script[data-template="item__details"]').text().split(/\$\{(.+?)\}/g);

	return {
		/**
		 * Devuelve un único producto de la sesión, en posición $key
		 */
		getSingleProduct: function(key){
			return JSON.parse(window.sessionStorage.getItem(key));
		},
		/**
		 * Devuelve array con N productos (determinado por count), comenzando
		 * desde posición key
		 */
		getMultipleProduct: function(ini_key, count){
			var objs = [];
			for(var i=ini_key;i<ini_key+count;i++){
				objs.push(this.getSingleProduct(i));
			}
			return objs;
		},
		/**
		 * Devuelve array con N productos (determinado por count), comenzando
		 * desde posición lastkey (último motrado con scroll infinito)
		 */
		getContinuousMultipleProduct: function(count){
			var objs = [];
			var i=lastkey
			for(;i<lastkey+count;i++){
				// Comprobamos que no está vacío antes de insertar
				var sp = this.getSingleProduct(i);
				if( sp !== null ){
					objs.push(sp);
				}
			}
			lastkey = i;

			return objs;
		},
		/**
		 * Inserta listado de productos usando el template
		 * (scroll infinito)
		 */
		insertProductListToHtml: function(product_list){
			var renders = function(item) {
  				return function(tok, i) { return (i % 2) ? item[tok] : tok; };
			}
			var items = [];
			$.each( product_list, function(index, val){
				// Si el producto no trae imágenes, mostramos un placeholder
				// en este caso, un gatito.
				// ¿A quién no le gustan los gatitos?
				var primary = (val.images.length && typeof val.images[0] !== undefined ) ? val.images[0].src : 'https://placekitten.com/1500/750';
				var secondary = (val.images.length && typeof val.images[1] !== undefined ) ? val.images[1].src : 'https://placekitten.com/1500/750';
				items.push({
					key: val.index,
					name: val.title,
					image_primary: primary,
					image_secondary: secondary,
				});
			});
			$('#product-list').append(items.map(function(item){
				return template.map(renders(item)).join('');
			}));
		},
		/**
		 * Inserta un solo producto
		 * popup
		 */
		insertSingleProductToHtml: function(selector,item){
			var renders = function(item) {
  				return function(tok, i) { return (i % 2) ? item[tok] : tok; };
			}

			// Si el producto no trae imágenes, mostramos un placeholder,
			// de nuevo, un gatito.
			var primary = (item.images.length && typeof item.images[0] !== undefined ) ? item.images[0].src : 'https://placekitten.com/1500/750';
			var items = [{
					key: item.index,
					name: item.title,
					image_primary: primary
			}];
			selector.html(items.map(function(item){
				return templateBig.map(renders(item)).join('');
			}));
		}
	}
})();

$(document).ready(function(){
	// Insertamos por defecto los 10 primeros productos
	Products.insertProductListToHtml(Products.getContinuousMultipleProduct(10));
})
