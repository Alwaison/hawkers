/**
 * Modal
 */
;$(document).ready(function(){
	
	/**
	 * Acciones para ocular el modal
	 */
	function hide_modal(){
		$('body').removeClass('modal-open');
		var modal = $('#modal');
		modal.find('.modal__content').html();
		modal.find('.modal__content').removeClass('showed');
		modal.find('.product-view__image').trigger('zoom.destroy'); // Eliminamos zoom
		modal.off('click');
	}

	/**
	 * Click en elemento que abre el modal
	 */
	$(document).on('click', "[data-modal='true']", function(e){
		e.preventDefault();
		var modal = $('#modal');
		
		// Obtenemos el producto a cargar en el modal, y lo insertamos
		var p = Products.getSingleProduct($(this).attr('data-product'));
		Products.insertSingleProductToHtml($('#modal').find('.modal__content'), p);

		// Zoom en el producto
		var image = modal.find('.product-view__image');
		image.zoom({url: image.find('img').attr('src')});

		// Mostramos el modal una vez cargado el producto
		$('body').addClass('modal-open');
		modal.find('.modal__content').addClass('showed');
		modal.on('click', function(){
			hide_modal();
		});
	});

	/**
	 * Click en botón para cerrar el modal
	 */
	$(document).on('click', "[data-dismiss='modal']", function(e){
		e.preventDefault();
		hide_modal();
	});

	/**
	 * Cualquier otro click en elementos del modal que no sea el botón de cerrar,
	 * cortamos la propagación del evento click
	 */
	$('.modal__dialog > *:not([data-dismiss="modal"])').on('click', function(e){
		e.stopPropagation();
	});

});