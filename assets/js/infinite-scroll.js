/**
 * Script para el scroll infinito durante la carga de productos
 */
;(function($, window, undefined) {
    var InfiniteScroll = function() {

        this.previousScroll = 0;
        this.productEnd = false;

        this.initialize = function() {
            this.setupEvents();
        };

        this.setupEvents = function() {
            $(window).on(
                'scroll',
                this.handleScroll.bind(this)
            );
        };

        this.handleScroll = function() {
            var scrollTop = $(document).scrollTop();
            if( scrollTop > this.previousScroll && this.previousScroll !== 0 ){
                var windowHeight = window.innerHeight;
                var height = $(document).height() - windowHeight;
                var scrollPercentage = (scrollTop / height);
                // Si el scroll es más del 95% de la página, forzamos el update
                if(scrollPercentage > 0.95) {
                    this.scrollAction();
                }
            }
            this.previousScroll = scrollTop;
        }

        this.scrollAction = function() {
            // Cargamos productos mientras sigan habiendo
            if( !this.productEnd ){
                var result = Products.getContinuousMultipleProduct(10);
                // Añadimos más resultados si siguen quedando productos
                if( result.length > 0 ){
                    Products.insertProductListToHtml(result);
                } else {
                    // Si no quedan mas productos, marcamos para no procesar mas
                    this.productEnd = true;
                }
            }
        }

        this.initialize();
    }

    $(document).ready(
        function() {
            // Initialize scroll
            new InfiniteScroll();
        }
    );
})(jQuery, window);
